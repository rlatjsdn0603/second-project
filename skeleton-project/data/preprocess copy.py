import os
import csv
import numpy as np
import pandas as pd
import random  # 나중에 데이터셋 섞어야되면 이거 쓸라고
from sklearn.model_selection import train_test_split


# Req. 3-1	이미지 경로 및 캡션 불러오기


def get_path_caption(img_path, captions_path):
    img_DIR = img_path
    cap_DIR = captions_path

    # 판다스 이용해서 파일 읽어오고 잘라버림
    # 한글인코딩, csv파일의 구분자, 맨윗줄 제거
    csv = pd.read_csv(cap_DIR, encoding="cp949",
                      sep="|", skiprows=1,
                      names=['image_name', 'comment_number', 'comment'])

    img_paths = csv['image_name'].values.tolist()  # names에서 뽑아온 csv를 리스트로 변경
    captions = csv['comment'].values.tolist()
    # print(img_paths.values.tolist())
    # print(captions.values.tolist())

    return img_paths, captions


def list_chunk(lst, n):
    # 리스트를 원하는 간격으로 나누는 메소드 -> https://jsikim1.tistory.com/141
    return [lst[i:i+n] for i in range(0, len(lst), n)]

# Req. 3-2	전체 데이터셋을 분리해 저장하기


def dataset_split_save(img_paths, captions, train_split):

    #  나중에 섞어야되면 여기서 섞는게 좋을거같기도하고
    # random.shuffle(img_paths)
    # random.shuffle(captions)
    #  각각 퍼센트기준으로 나뉜 트레이닝,테스트 데이터 리스트들
    # train_img = img_paths[:int(split_size)]
    # test_img = img_paths[int(split_size):]
    # train_captions = captions[:int(split_size)]
    # test_captions = captions[int(split_size):]
    # print(len(train_img))
    # print(len(test_img))
    # 위는 리스트로 하는건데 아래부터 딕셔너리로 다시 할 예정

    print('입력된 트레이닝데이터셋 % : ' + str(train_split) + "%")

#   print("기본 캡션 : " + str(captions[:5]))
    list_chunked = list_chunk(captions, 5)  # 5개씩 캡션을 묶어서 하나의 리스트로 변경
#   print("5개씩 나눈 캡션 " + str(list_chunked[:5]))

    # for문으로 하니까 너무 느려서 아래 방법으로함
    # new_list = []
    # for v in img_paths:
    #     if v not in new_list:
    #         new_list.append(v)

    result1 = dict.fromkeys(img_paths)  # 리스트 값들을 key 로 변경 -> 중복 제거
    result2 = list(result1)  # list(dict.fromkeys(arr)) -> 리스트로 변경

    dic = dict(zip(result2, list_chunked))  # 중복이 제거된 리스트들로 다시 딕셔너리 생성

    # 만든 딕셔너리를 셔플 -> 셔플하면 리스트로 바뀜
    dic = sorted(dic.items(), key=lambda x: random.random())

    split_size = len(dic) * (train_split/100)
#    print('나눈 크기는 : ' + str(split_size))

    # 섞인 딕셔너리를 스플릿사이즈에 맞게 나누어 트레이닝과 테스트로 나눔
    train_tt = dic[:int(split_size)]
    test_tt = dic[int(split_size):]

#    print("트레이닝 : " + str(train_tt[:2]))
#    print("테스트 : " + str(test_tt[:2]))

    # 나누어진 데이터를 csv 파일로 저장
    # https://www.delftstack.com/ko/howto/python/write-list-to-csv-python/
    df = pd.DataFrame(train_tt)
    df.to_csv('./datasets/train_val.csv', sep=",")
    df = pd.DataFrame(test_tt)
    df.to_csv('./datasets/test_val.csv', sep=",")

    train_dataset_path = './datasets/train_val.csv'
    val_dataset_path = './datasets/test_val.csv'
    return train_dataset_path, val_dataset_path


# Req. 3-3	저장된 데이터셋 불러오기
def get_data_file(do_traning, train_dataset_path, val_dataset_path):

    if do_traning:
        print("학습용데이터에요")

        csv = pd.read_csv(train_dataset_path,
                          sep=",", skiprows=1,
                          names=['0', '1'])

        img_paths = []
        captions = []
        img_paths = csv['0'].values.tolist()
        captions = csv['1'].values.tolist()
        # print(img_paths.values.tolist())
        # print(captions.values.tolist())

#        print("이미지만나와야함 : " + str(img_paths[0]))
#        print("캡션만나와야함 : " + str(captions[0]))

        return img_paths, captions

    else:
        print("테스트용 데이터입니당")
        csv = pd.read_csv(val_dataset_path,
                          sep=",", skiprows=1,
                          names=['0', '1'])

        img_paths = []
        captions = []
        img_paths = csv['0'].values.tolist()
        captions = csv['1'].values.tolist()
        # print(img_paths.values.tolist())
        # print(captions.values.tolist())

#        print("이미지만나와야함 : " + str(img_paths[0]))
#        print("캡션만나와야함 : " + str(captions[0]))
        return img_paths, captions


# Req. 3-4	데이터 샘플링
def sampling_data(img_paths, caption, sampling_split):

    print('입력된 트레이닝데이터셋 % : ' + str(sampling_split) + "%")

    print("전체 데이터 개수 : " + str(len(img_paths)))
    split_size = len(img_paths) * (sampling_split/100)
    #print('나눈 크기는 : ' + str(split_size))

    img_paths = img_paths[:int(split_size)]
    caption = caption[:int(split_size)]

    print("샘플링된 데이터 개수 : " + str(len(img_paths)))
    print("샘플링된 데이터 퍼센트 : " + str(sampling_split))
    return img_paths, caption
