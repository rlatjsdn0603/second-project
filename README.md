프론트엔드 → Vue, 포트 : 8080

npm install
npm run serve


백엔드 → Django , 포트 : 8000

pip install -r requirements.txt
conda env create -f conda_requirements.txt

python manage.py migrate
python manage.py runserver

DB → Mysql

포트 : 3306


스웨거 링크 : [http://127.0.0.1:8000/swagger/](http://127.0.0.1:8000/swagger/)